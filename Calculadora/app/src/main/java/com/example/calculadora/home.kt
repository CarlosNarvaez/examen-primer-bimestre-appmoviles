package com.example.calculadora

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.findNavController
import kotlinx.android.synthetic.main.fragment_home.*


class home : Fragment() {

    internal lateinit var B1:Button
    internal lateinit var B2:Button
    internal lateinit var B3:Button
    internal lateinit var B4:Button
    internal lateinit var mas:String
    internal lateinit var menos:String
    internal lateinit var por:String
    internal lateinit var div:String


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        B1=view.findViewById(R.id.button)
        mas="+"
        B2=view.findViewById(R.id.button2)
        menos="-"
        B3=view.findViewById(R.id.button3)
        por="*"
        B4=view.findViewById(R.id.button4)
        div="/"

        B1= view.findViewById(R.id.button)
        B1.setOnClickListener { goto(mas) }


        B2= view.findViewById(R.id.button2)
        B2.setOnClickListener { goto(menos) }


        B3= view.findViewById(R.id.button3)
        B3.setOnClickListener { goto(por) }


        B4= view.findViewById(R.id.button4)
        B4.setOnClickListener { goto(div) }
    }

    fun goto(signoo: String){
        val action= homeDirections.actionHome2ToOperaciones(signoo)
        view?.findNavController()?.navigate(action)
    }









    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }



    companion object {

    }
}
