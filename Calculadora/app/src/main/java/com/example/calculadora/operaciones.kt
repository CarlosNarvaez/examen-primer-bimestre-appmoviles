package com.example.calculadora

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast


class operaciones : Fragment() {


    internal lateinit var welcomMessage: TextView
    internal lateinit var gameScoreTextView: TextView
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var goButton: Button


    var score=0


    var gameStarted = false

    private var initialCountDown: Long = 10000
    private var countDownInterval: Long =100
    private lateinit var countDownTimer: CountDownTimer // para implementar la varibale timer


    var timeLeft = 10;


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        gameScoreTextView= view.findViewById(R.id.scoree)
        goButton = view.findViewById(R.id.enviarr)
        timeLeftTextView = view.findViewById(R.id.time)

        // guardar el estado de nuestras variables y poderlas reutilizar nuestras variables
        /* if (savedInstanceState != null){
             score=savedInstanceState.getInt(SCORE_KEY)
         }*/
        //gameScoreTextView.text =getString(R.string.score_i,score)
        gameScoreTextView.text=getString(R.string.score_d,score)
        timeLeftTextView.text =getString(R.string.timeleft_d, timeLeft)



        goButton.setOnClickListener { incrementScore()}
        resetGame()

        if(gameStarted){
            resetGame()
            return
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_operaciones, container, false)
    }





    companion object {

    }


    fun incrementScore(){


        if(!gameStarted){
            countDownTimer.start()

        }

        score ++
        gameScoreTextView.text = getString(R.string.score_d, score)
    }

    private fun resetGame(){
        score= 0
        gameScoreTextView.text =getString(R.string.score_d,score)

        timeLeft = 10
        timeLeftTextView.text =getString(R.string.timeleft_d,timeLeft)

        countDownTimer= object :CountDownTimer(initialCountDown,countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text =getString(R.string.timeleft_d, timeLeft)
            }

            override fun onFinish() {


            }

        }
    }

    private fun startGame(){

        countDownTimer.start()
        gameStarted = true

    }

    private fun restoreGame(){
        gameScoreTextView.text =getString(R.string.score_d,score)

        countDownTimer= object :CountDownTimer
            (timeLeft * 1000L,countDownInterval){

            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt() / 1000
                timeLeftTextView.text =getString(R.string.timeleft_d, timeLeft)
            }

            override fun onFinish() {


            }

        }
        countDownTimer.start()
    }




}
